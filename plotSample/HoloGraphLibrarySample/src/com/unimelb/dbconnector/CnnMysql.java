package com.unimelb.dbconnector;

/**
 * Created by yingying on 8/30/2015.
 */
import java.sql.*;

public class  CnnMysql{
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://127.0.0.1/app_data";

    //  Database credentials
    static final String USER = "wangyingying";
    static final String PASS = "123456";
    static final String DB = "app_data";

    private  Connection conn = null;
    private Statement stmt = null;

    public void getMysqlConnection() {
        try {
            Class.forName(JDBC_DRIVER);
            if (conn == null) {
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public ResultSet query(String sql) throws SQLException {
        getMysqlConnection();
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public Boolean execute(String sql) {
        getMysqlConnection();
        try {
            stmt = conn.createStatement();
            stmt.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void readData(ResultSet rs) throws SQLException {
        try {
            while(rs.next()) {
                //Retrieve by column name
             /*step1: read data from usb,physical device */
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                System.out.println("name:"+name);
                System.out.println("id:"+id);
                System.out.println("tel:"+tel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            if(stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) throws SQLException {
        CnnMysql mysql = new CnnMysql();
        ResultSet rs = mysql.query("SELECT * FROM user_info LIMIT 100");
        mysql.readData(rs);
//        for(int i = 500000; i < 505000; i++) {
//            int id= i;
//            String user = "honey_"+i;
//            long tel = 15653932143L+i;
//            String sql = "INSERT INTO user_info VALUES ("+id+",'"+user+"',"+tel+")";
////            System.out.println(sql);
//            mysql.execute(sql);
//        }
//        mysql.readData(rs);
        mysql.close();
    }//end main
}//end